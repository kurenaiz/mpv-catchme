local config = require('config')
local utils = require('utils')
local ph = require('placeholder')
local sql3 = require('lsqlite3')

local db_exists = utils.file_exists(config.db_path)
local db = sql3.open(config.db_path)
local should_auto_write_playlist = false
local default_volume = mp.get_property_native('volume')
local fade_in_timer = nil

local function notify(message)
	print(message)
	mp.command_native_async({
		name = 'subprocess',
		playback_only = false,
		args = { 'sh', '-c', string.format([[notify-send -i "%s" "Catchme" "%s"]], config.icon_path, message) }
	})
end

local function on_file_loaded(_)
	if config.notify then
		local suc, cmd = ph.sub_placeholder(config.on_file_change)
		if not suc then
			local filename = mp.get_property_native('filename')
			cmd = ph.sub_placeholder_fallback(config.on_file_change_fallback, filename)
		end
		print(cmd)
		mp.command_native_async({
			name = 'subprocess',
			playback_only = false,
			args = { 'sh', '-c', cmd }
		})
	end
end

local function write_playlist_paths(playlist)
	local t = {}
	for i = 1, #playlist do
		table.insert(t, playlist[i].filename)
	end
	local out_paths = table.concat(t, "\n") .. "\n"

	local paths_fd, err = io.open(os.getenv('XDG_CONFIG_HOME') .. "/catchme/music_paths", 'w')
	if paths_fd then
		paths_fd:write(out_paths)
		paths_fd:close()
	else
		notify("ERROR: paths:" .. err)
	end
end

local function write_playlist_names(playlist)
	local t = {}
	for i = 1, #playlist do
		local found = false
		for row in db:nrows("SELECT * FROM songs") do
			if playlist[i].filename == row.url then
				table.insert(t, ph.sub_placeholder_db(row))
				found = true
				break
			end
		end
		if not found then
			local tags = ph.get_ffprobe_tags(playlist[i].filename)
			if tags then
				db:exec(ph.format_song_db_insert(tags));
				table.insert(t, ph.sub_placeholder_tags(tags))
			end
		end
	end

	local out_names = table.concat(t, "\n") .. "\n"
	local names_fd, err_n = io.open(os.getenv('XDG_CONFIG_HOME') .. "/catchme/music_names", 'w')
	if names_fd then
		names_fd:write(out_names)
		names_fd:close()
	else
		notify("ERROR: names:" .. err_n)
	end
end

local function on_shutdown(_)
	if config.save_playlist_on_quit then
		local playlist = mp.get_property_native('playlist')
		write_playlist_paths(playlist)
		local pos = mp.get_property_native('playlist-pos')
		local index, err = io.open(os.getenv('XDG_CONFIG_HOME') .. "/catchme/playlist_index", 'w')
		if index then
			index:write(pos)
			index:close()
		else
			notify("ERROR: paths:" .. err)
		end
	end
end

local function filter_playlist()
	local playlist = mp.get_property_native('playlist')

	local deleted = 0;
	for i = 1, #playlist do
		if utils.endswith(playlist[i].filename, 'jpg') or
		    utils.endswith(playlist[i].filename, 'cue') or
		    utils.endswith(playlist[i].filename, 'png') then
			mp.commandv("playlist-remove", (playlist[i].id - deleted - 1));
			deleted = deleted + 1
		end
	end
end

local function on_write_playlist_cmd(_)
	notify("Writing playlist")
	local playlist = mp.get_property_native('playlist')
	write_playlist_paths(playlist)
	write_playlist_names(playlist)
end

local function updatedb()
	notify(string.format([[updating db at %s]], config.db_path))
	local playlist = mp.get_property_native('playlist')
	local transaction = "BEGIN TRANSACTION;"
	for i = 1, #playlist do
		local tags = ph.get_ffprobe_tags(playlist[i].filename)
		transaction = transaction .. ph.format_song_db_insert(tags)
	end
	transaction = transaction .. "COMMIT;"
	db:exec(transaction)
	if db:error_code() ~= 0 then
		notify([[ERROR: while updating db: ]] .. db:error_message())
	end

	print('SUCCESS: updatedb finished')
	if config.auto_write_playlist then
		notify("Writing playlist")
		write_playlist_paths(playlist);
		write_playlist_names(playlist);
	end
end

local function on_updatedb_cmd(_)
	updatedb()
end

local function on_end_file(_)
	if config.auto_write_playlist and should_auto_write_playlist then
		notify("Writing playlist")
		should_auto_write_playlist = false

		local playlist = mp.get_property_native('playlist')
		write_playlist_paths(playlist);
		write_playlist_names(playlist);
	end
end

local function on_pause(_, paused)
	if config.fade_in then
		if paused then
			local vol = mp.get_property_native('volume')
			mp.set_property("volume", 0)
			print(fade_in_timer)
			if fade_in_timer then
				fade_in_timer:kill();
				fade_in_timer = nil
			else
				default_volume = vol
				print(default_volume)
			end
		else
			if fade_in_timer then
				fade_in_timer:resume()
			else
				fade_in_timer = mp.add_periodic_timer(0.01, function()
					local curr_volume = mp.get_property_native('volume')
					if curr_volume < default_volume then
						mp.set_property("volume", curr_volume + 1)
					else
						print("killing")
						fade_in_timer:kill()
						fade_in_timer = nil
					end
				end)
			end
		end
	end
end

-- local function on_playlist_cmd(_)
-- filter_playlist()
-- mp.command("playlist")
-- end

-- MAIN

if (not db_exists) then
	print("creating db at " .. config.db_path)

	db:exec [=[
	  CREATE TABLE songs(
	    title TEXT DEFAULT '',
	    album TEXT DEFAULT '',
	    artist TEXT DEFAULT '',
	    albumartist TEXT DEFAULT '',
	    track INTEGER NOT NULL DEFAULT -1,
	    disc INTEGER NOT NULL DEFAULT -1,
	    date TEXT DEFAULT '',
	    genre TEXT DEFAULT '',
	    composer TEXT DEFAULT '',
	    performer TEXT DEFAULT '',
	    comment TEXT DEFAULT '',
	    art TEXT DEFAULT '',
	    url TEXT NOT NULL DEFAULT '' UNIQUE
	);
	DROP INDEX IF EXISTS "idx_album";
	CREATE INDEX idx_album ON songs (album);
	DROP INDEX IF EXISTS "idx_albumartist";
	CREATE INDEX idx_albumartist ON songs (albumartist);
	DROP INDEX IF EXISTS "idx_artist";
	CREATE INDEX idx_artist ON songs (artist);
	DROP INDEX IF EXISTS "idx_comp_artist";
	CREATE INDEX idx_title ON songs (title);
	DROP INDEX IF EXISTS "idx_url";
	CREATE INDEX idx_url ON songs (url);
	]=]
	if db:error_code() ~= 0 then
		notify(db:error_message())
	end
	updatedb()
else
	should_auto_write_playlist = true
end

if config.filter_playlist then
	filter_playlist();
end

mp.register_script_message('updatedb', on_updatedb_cmd)
mp.register_script_message('write-playlist', on_write_playlist_cmd)
mp.register_event('file-loaded', on_file_loaded)
mp.register_event('start-file', on_end_file)
mp.register_event('shutdown', on_shutdown)
mp.observe_property("pause", "bool", on_pause)
