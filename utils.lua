local utils = {}

function utils.get_file_name(path)
	return path:match("^.+/(.+)$")
end

function utils.file_exists(name)
	local f = io.open(name, "r")
	if f ~= nil then io.close(f) return true else return false end
end

function utils.escape_path(name)
	-- escape all double quotes
	local out = string.gsub(name, [["]], [[\"]])
	out = string.gsub(out, [[']], [[\']])
	out = string.gsub(out, [[%(]], [[\(]])
	out = string.gsub(out, [[%)]], [[\)]])
	out = string.gsub(out, [[% ]], [[\ ]])
	out = string.gsub(out, '%&', '\\&')
	out = string.gsub(out, '%;', '\\;')
	out = string.gsub(out, '%[', '\\[')
	out = string.gsub(out, '%]', "\\]")
	return out
end

function utils.escape_string(str)
	return str:gsub([["]], [[\"]])
end

function utils.escape_sqlite(str)
	return str:gsub([[']], [['']])
end

function utils.endswith(str, ending)
    return ending == "" or str:sub(-#ending) == ending
end

function utils.dump(o)
	if type(o) == 'table' then
		local s = '{ '
		for k, v in pairs(o) do
			if type(k) ~= 'number' then k = '"' .. k .. '"' end
			s = s .. '[' .. k .. '] = ' .. utils.dump(v) .. ','
		end
		return s .. '} '
	else
		return tostring(o)
	end
end

return utils
