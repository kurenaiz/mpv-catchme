local options = require('mp.options')
local utils = require('utils')

local config = {
	notify = true,
	db_path = "default", -- XDG_DATA_HOME/catchme/music.db OR .local/share/catchme/music.db
	icon_path = "./",
	filter_playlist=false,
	auto_write_playlist=true,
	save_playlist_on_quit = false,
	on_file_change = 'notify-send -i "%cover" "playing" "%album - %title"',
	fade_in=true,
	on_file_change_fallback = 'notify-send -i "%cover"  "playing" "%filename"',
	name_format = '%album - %title',
	placeholder_fallback = 'N/A',
	-- TODO
	placeholder_map = 'artist=N/A;title=N/A;cover=;genre=;comment=;',
}

options.read_options(config, 'mpv-catchme')

if config.db_path == "default" then
	local datahome = os.getenv('XDG_DATA_HOME')
	if datahome then
		config.db_path = datahome .. "/catchme/"
	else
		config.db_path = os.getenv('HOME') .. "/.local/share/catchme/"
	end
	if not utils.file_exists(config.db_path) then
		os.execute("mkdir " .. config.db_path .. " &>/dev/null")
	end
	config.db_path = config.db_path .. "music.db"
end

return config
