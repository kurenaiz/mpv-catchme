# mpv-catchme

Adds music server like features to mpv.
it enables features like:

- [X] Execute command on file change (for notifications)
- [X] Fade-in audio when unpausing
- [X] Write playlist file paths and formatted names to disk. (to use with other programs like dmenu)
- [ ] Better playlist file support

what more?

## Dependencies

lua51-lsqlite3

## Commands
```shell
# this updates the whole db with up-to-date information (song list and song metadata).
# catchme automatically caches the metadata of new songs to the DB, 
# but there's no good way to now if the metadata of a song changed,
# so you might want to manually run this when you update the metadata of a song
$ catchme --cm updatedb
# writes the currently playing songs to music_paths and music_names (to use with dmenu, check catchmenu), read CONFIGURATION section for more info.
$ catchme --cm write-playlist
```

## Installation

clone this repository inside the mpv scripts folder.

On Linux:

```
~/.config/mpv/scripts/scripts.disable/mpv-catchme/main.lua
```
by placing it inside scripts.disable, it will not load by default,
now you need to load it manually, like so:

```
mpv --script=~/.config/mpv/scripts/scripts.disable/mpv-catchme
```

the `catchme` program provides a `start` script which already loads the script inside scripts.disable.

## Configuration

copy the `mpv-catchme.conf` at this folder to `~/.config/mpv/script-opts/mpv-catchme.conf` 
uncomment and edit the values accordingly.

## FAQ

### My musics are not properly equalized, can catchme remember the volume of a music?

No, maybe in the future, but you can add this filter to your start script, which tries to
normalized the loudness of every file.
```
--af="lavfi=[loudnorm=I=-16:TP=-3:LRA=4]"
```

### See Also

[catchme](https://gitlab.com/kurenaiz/catchme) - mpv controller

[catchmenu](https://gitlab.com/kurenaiz/catchmenu) - dmenu script for selecting music

[catchme-scripts](https://gitlab.com/kurenaiz/catchme-scripts) - general scripts which use catchme
