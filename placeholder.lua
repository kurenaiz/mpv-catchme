local utils = require('utils')
local config = require('config')
local mp_utils = require('mp.utils')

local placeholder = {}

local placeholders = {
	ARTIST = "%%artist",
	GENRE = "%%genre",
	TITLE = "%%title",
	ALBUM = "%%album",
	COVER = "%%cover",
	FILENAME = "%%filename",
	FILEPATH = "%%filepath",
};

function placeholder.get_ffprobe_data(filename)
	-- query for tags
	local cmd =
	    [[ffprobe -hide_banner -show_streams -v quiet -print_format json -show_format -show_entries format=stream_tags:format_tags ]]
	    ..
	    utils.escape_path(filename)
	local handle = io.popen(cmd)
	local metadata
	if handle then
		local json = handle:read("*a")
		metadata = mp_utils.parse_json(json)
		handle:close()
	end
	return metadata
end

-- get ffprobe'd tags of the given `filepath`
function placeholder.get_ffprobe_tags(filepath)
	local metadata = placeholder.get_ffprobe_data(filepath)

	-- look for tags inside the `format` field
	local tags
	local format = metadata['format']
	if not format then
		tags = metadata['tags']
	else
		tags = format['tags']
	end

	-- if there's no tags inside `format`, we look for it inside `streams`
	if not tags then
		local streams = metadata['streams']
		for _, v in pairs(streams) do
			tags = v['tags']
			if tags then
				break
			end
		end
	end

	if not tags then
		tags = {}
	end

	tags['url'] = filepath
	return tags
end

-- returns formatted sqlite INSERT with the given tags
function placeholder.format_song_db_insert(tags)
	if not tags then
		tags = {}
	end
	tags['title'] = utils.escape_sqlite(tags['title'] or tags['TITLE'] or tags['Title'] or "")
	tags['artist'] = utils.escape_sqlite(tags['artist'] or tags['ARTIST'] or tags['Artist'] or "")
	tags['genre'] = utils.escape_sqlite(tags['genre'] or tags['GENRE'] or tags['Genre'] or "")
	tags['album'] = utils.escape_sqlite(tags['album'] or tags['ALBUM'] or tags['Album'] or "")
	-- tags['albumartist'] = utils.escape_sqlite(tags['albumartist'] or tags['ALBUMARTIST'] or tags['AlbumArtist'] or "")
	-- tags['performer'] = utils.escape_sqlite(tags['performer'] or tags['PERFORMER'] or tags['Performer'] or "")
	-- tags['composer'] = utils.escape_sqlite(tags['composer'] or tags['COMPOSER'] or tags['Composer'] or "")
	tags['track'] = utils.escape_sqlite(tags['track'] or tags['TRACK'] or tags['Track'] or "")
	tags['date'] = utils.escape_sqlite(tags['date'] or tags['DATE'] or tags['Date'] or "")
	tags['url'] = utils.escape_sqlite(tags['url'] or "")
	return string.format([[
		INSERT INTO songs(title, album, artist, genre, track, date, url) VALUES(
		        '%s',
		        '%s',
		        '%s',
		        '%s',
		        '%s',
		        '%s',
		        '%s')
		ON CONFLICT(url) DO UPDATE SET
		        title = excluded.title,
			artist = excluded.artist,
			genre = excluded.genre,
			date = excluded.date,
			track = excluded.track,
			url = excluded.url,
			album = excluded.album;]], tags['title'], tags['album'], tags['artist'], tags['genre'], tags['track'], tags['date'],
		tags['url'])
end

function placeholder.sub_placeholder_ffprobe(str, filepath)
	local tags = placeholder.get_ffprobe_tags(filepath)
	if tags == nil then
		str = "INVALID"
		return str
	end

	local title, artist, album, genre
	if tags then
		title = tags['title'] or tags['TITLE'] or tags['Title']
		artist = tags['artist'] or tags['ARTIST'] or tags['Artist']
		genre = tags['genre'] or tags['GENRE'] or tags['Genre']
		album = tags['album'] or tags['ALBUM'] or tags['Album']
	end

	str = str:gsub(placeholders.TITLE, title and title or config.placeholder_fallback)
	str = str:gsub(placeholders.ARTIST, artist and artist or config.placeholder_fallback)
	str = str:gsub(placeholders.ALBUM, album and album or config.placeholder_fallback)
	str = str:gsub(placeholders.GENRE, genre and genre or config.placeholder_fallback)
	str = str:gsub(placeholders.FILENAME, utils.get_file_name(filepath))
	str = str:gsub(placeholders.FILEPATH, filepath)

	return str
end

-- substitute PLACEHOLDERS from str
function placeholder.sub_placeholder(str)
	local titlep, _ = str:find(placeholders.TITLE)
	local artistp, _ = str:find(placeholders.ARTIST)
	local albump, _ = str:find(placeholders.ALBUM)
	local genrep, _ = str:find(placeholders.GENRE)
	local fnamep, _ = str:find(placeholders.FILENAME)
	local coverp, _ = str:find(placeholders.COVER)
	-- local s_c, _ = str:find("%c")
	local success = false

	if titlep then
		local title = mp.get_property_native('metadata/by-key/title')
		if title then
			success = true
			title = utils.escape_string(title)
			str = str:gsub(placeholders.TITLE, title)
		else
			str = str:gsub(placeholders.TITLE, config.placeholder_fallback)
		end
	end

	if artistp then
		local artist = mp.get_property_native('metadata/by-key/artist')
		if artist then
			success = true
			artist = utils.escape_string(artist)
			str = str:gsub(placeholders.ARTIST, artist)
		else
			str = str:gsub(placeholders.ARTIST, config.placeholder_fallback)
		end
	end

	if albump then
		local album = mp.get_property_native('metadata/by-key/album')
		if album then
			success = true
			album = utils.escape_string(album)
			str = str:gsub(placeholders.ALBUM, album)
		else
			str = str:gsub(placeholders.ALBUM, config.placeholder_fallback)
		end
	end

	if fnamep then
		local filepath = mp.get_property_native('filename')
		success = true
		str = str:gsub(placeholders.FILENAME,
			filepath and utils.get_file_name(filepath) or config.placeholder_fallback)
	end

	if genrep then
		local genre = mp.get_property_native('metadata/by-key/genre')
		success = true
		str = str:gsub(placeholders.GENRE, genre and genre or config.placeholder_fallback)
	end

	if coverp then
		local track_list = mp.get_property_native("track-list")
		for _, track in ipairs(track_list) do
			if track.albumart and track.external then
				str = str:gsub(placeholders.COVER, track['external-filename'])
				break
			end
		end
	end

	return success, str
end

-- substitute only fallback PLACEHOLDERS from str
function placeholder.sub_placeholder_fallback(str, filename)
	local s_cover, _ = str:find(placeholders.COVER)
	local s_fname, _ = str:find(placeholders.FILENAME)
	if s_fname then
		str = str:gsub(placeholders.FILENAME, filename and filename or config.placeholder_fallback)
	end

	if s_cover then
		local track_list = mp.get_property_native("track-list")
		for _, track in ipairs(track_list) do
			if track.albumart and track.external then
				str = str:gsub(placeholders.COVER, track['external-filename'])
				break
			end
		end
	end

	return str
end

function placeholder.sub_placeholder_tags(tags)
	if not tags then
		return
	end

	local str = config.name_format

	str = str:gsub(placeholders.TITLE, tags['title'] or config.placeholder_fallback)
	str = str:gsub(placeholders.ARTIST, tags['artist'] or config.placeholder_fallback)
	str = str:gsub(placeholders.ALBUM, tags['album'] or config.placeholder_fallback)
	str = str:gsub(placeholders.GENRE, tags['genre'] or config.placeholder_fallback)
	str = str:gsub(placeholders.FILENAME, utils.get_file_name(tags['url']))
	str = str:gsub(placeholders.FILEPATH, tags['url'])

	return str
end

function placeholder.sub_placeholder_db(row)
	local str = config.name_format

	str = str:gsub(placeholders.TITLE, row.title or config.placeholder_fallback)
	str = str:gsub(placeholders.ARTIST, row.artist or config.placeholder_fallback)
	str = str:gsub(placeholders.ALBUM, row.album or config.placeholder_fallback)
	str = str:gsub(placeholders.GENRE, row.genre or config.placeholder_fallback)
	-- str = str:gsub(placeholders.COVER, row.cover or '')
	str = str:gsub(placeholders.FILENAME, utils.get_file_name(row.url))
	str = str:gsub(placeholders.FILEPATH, row.url)

	return str
end

return placeholder
